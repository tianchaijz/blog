Emacs
=====

#### From Vim To Emacs
- Browse code(Nerd tree)
- Tag list
- Jump(evil)
- External command
- Key bindings(tee, trailing space ...)
- Git
- DHS (Directory hierarchy standard)
- Clipboard
- Align


#### References
```
https://github.com/rafrombrc/dot-emacs-dot-d.git
https://github.com/redguardtoo/emacs.d.git
https://github.com/purcell/emacs.d
```


#### Complete the docs
| key bindings                         | explanation                                                              |
|--------------------------------------+--------------------------------------------------------------------------|
| M-x shell-command-on-region(M-\vert) | send current region to shell                                             |
| C-u M-! <shell-command>              | insert command result into buffer                                        |
| C-u M-\vert  <shell-command>         | current region is sent to the shell-command and replaced with the result |
| M-^                                  | join line                                                                |
| M-g g                                | jump to the specified line                                               |
| M-x apply-macro-to-region-lines      | apply macro to region lines                                              |
| C-u C-space(or C-u C-@)              | go back to previous line position                                        |
| C-x C-space(or C-x C-@)              | go back to previous line position                                        |
| C-M-right \vert C-M-left             | go to the beginning or the end of the current expression (C-M-f, C-M-b)  |
...
